package entornosuml;

import java.util.ArrayList;
import java.util.Scanner;

public class Principal {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        int opc;
        ArrayList <Futbolista> futbolistasgeneral=new ArrayList();
        ArrayList <Delantero> delanteros=new ArrayList();
        ArrayList <Portero> porteros=new ArrayList();
        
        do{
            System.out.println("1.Insertar futbolista\n"
                    + "2.Modificar atributos futbolista\n"
                    + "3.Ocasion de gol \n"
                    + "4.Listar futbolitas\n"
                    + "5.Listar porteros\n"
                    + "6.Listar delanteros\n"
                    + "0.Salir\n"
                    + "Que desea hacer"); 
            opc=teclado.nextInt();
            switch(opc){
                case 1:
                    int elegirtipo;
                    System.out.println("1.Delantero 2.Portero");
                    elegirtipo=teclado.nextInt();
                    switch(elegirtipo){
                        case 1:
                            System.out.println("Dime nombre");
                            String nombre=teclado.next();
                            System.out.println("Dime apellido");
                            String apellido=teclado.next();
                            System.out.println("Dime altura");
                            int altura=teclado.nextInt();
                            System.out.println("Dime peso");
                            int peso=teclado.nextInt();
                            System.out.println("Dime nivel de tiro");
                            int tiro=teclado.nextInt();
                            System.out.println("Dime nivel de definicion");
                            int def=teclado.nextInt();
                            Delantero d=new Delantero(tiro,def,nombre,apellido,altura,peso);
                            delanteros.add(d);
                            futbolistasgeneral.add(d);
                            break;
                        case 2:
                            System.out.println("Dime nombre");
                            String nombrep=teclado.next();
                            System.out.println("Dime apellido");
                            String apellidop=teclado.next();
                            System.out.println("Dime altura");
                            int alturap=teclado.nextInt();
                            System.out.println("Dime peso");
                            int pesop=teclado.nextInt();
                            System.out.println("Dime nivel de paradas");
                            int paradas=teclado.nextInt();
                            System.out.println("Dime nivel de reflejos");
                            int reflejos=teclado.nextInt();
                            Portero p=new Portero(paradas,reflejos,nombrep,apellidop,alturap,pesop);
                            porteros.add(p);
                            futbolistasgeneral.add(p);
                            break;
                        default:
                            System.out.println("Opcion no valida");
                            break;
                    }
                    break;
                case 2:
                    int elegirtipo2;
                    System.out.println("1.Delantero 2.Portero");
                    elegirtipo2=teclado.nextInt();
                    switch (elegirtipo2){
                        case 1:
                            System.out.println("Dime nombre");
                            String nom=teclado.next();
                            System.out.println("Dime apellido");
                            String ape=teclado.next();
                            for (int i = 0; i < delanteros.size(); i++) {
                                if(delanteros.get(i).getNombre().equals(nom) && delanteros.get(i).getApellido().equals(ape)){
                                    System.out.println("1.Tiro 2.Definicion");
                                    int opcd=teclado.nextInt();
                                    if(opcd==1){
                                        System.out.println("Dime nuevo tiro");
                                        int nuevotiro=teclado.nextInt();
                                        delanteros.get(i).cambiartiro(nuevotiro);
                                    }
                                    else if(opcd==2){
                                        System.out.println("Dime nueva definicion");
                                        int nuevadef=teclado.nextInt();
                                        delanteros.get(i).cambiardefinicion(nuevadef);
                                    }
                                }
                            }
                        break;
                        case 2:
                            System.out.println("Dime nombre");
                            String nombrep=teclado.next();
                            System.out.println("Dime apellido");
                            String apellidop=teclado.next();
                            for (int i = 0; i < porteros.size(); i++) {
                                if(porteros.get(i).getNombre().equals(nombrep) && porteros.get(i).getApellido().equals(apellidop)){
                                    System.out.println("1.Paradas 2.Reflejos");
                                    int opcd=teclado.nextInt();
                                    if(opcd==1){
                                        System.out.println("Dime nuevas paradas");
                                        int nuevaparadas=teclado.nextInt();
                                        porteros.get(i).cambiarparadas(nuevaparadas);
                                    }
                                    else if(opcd==2){
                                        System.out.println("Dime nuevos reflejos");
                                        int ref=teclado.nextInt();
                                        porteros.get(i).cambiarreflejos(ref);
                                    }
                                    }
                    }
                    break;
                    }
                break;
                case 3:
                    Delantero d2=new Delantero(0,0,"","",0,0);
                    Portero p2=new Portero(0,0,"","",0,0);
                    System.out.println("Dime nombre delantero");
                    String nomd=teclado.next();
                    System.out.println("Dime apellido delantero");
                    String aped=teclado.next();
                    System.out.println("Dime nombre portero");
                    String nomp=teclado.next();
                    System.out.println("Dime apellido portero");
                    String apep=teclado.next();
                    for (int i = 0; i < futbolistasgeneral.size(); i++) {
                        if(nomd.equals(futbolistasgeneral.get(i).getNombre()) && aped.equals(futbolistasgeneral.get(i).getApellido())){
                       d2=(Delantero) futbolistasgeneral.get(i);
                        }
                        if(nomp.equals(futbolistasgeneral.get(i).getNombre()) && apep.equals(futbolistasgeneral.get(i).getApellido())){
                            p2=(Portero) futbolistasgeneral.get(i);
                        }
                    }
               d2.disparoapuerta(true);
                    if(d2.puntostotales()>p2.puntostotales()){
                        System.out.println("Gollll de "+d2.getNombre()+" "+d2.getApellido());
                    }
                    else{
                        System.out.println("Parada del portero "+p2.getNombre()+" "+p2.getApellido());
                    }
                    break;
                case 4:
                    for (int i = 0; i < futbolistasgeneral.size(); i++) {
                        System.out.println(futbolistasgeneral.get(i).toString());
                    }
                break;
                case 5:
                    for (int i = 0; i < porteros.size(); i++) {
                        System.out.println(porteros.get(i).toString());
                    }
                break;
                case 6:
                    for (int i = 0; i < delanteros.size(); i++) {
                        System.out.println(delanteros.get(i).toString());
                    }
                break;
                case 0:
                    System.out.println("Fin");
                    break;
                default:
                    System.out.println("Opcion no valida");
                    break;
            }
        }while(opc!=0);
        
    }
}

