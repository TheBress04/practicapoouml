package entornosuml;
public class Futbolista {
    private String nombre;
    private String apellido;
    private int altura;
    private double peso;

    public Futbolista(String nombre, String apellido, int altura, double peso) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.altura = altura;
        this.peso = peso;
    }
    
    //<editor-fold defaultstate="collapsed" desc="getters y setters">
      public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }
//</editor-fold>

    @Override
    public String toString() {
        return "Futbolista{" + "nombre=" + nombre + ", apellido=" + apellido + ", altura=" + altura + ", peso=" + peso + '}';
    }

  
    
}
