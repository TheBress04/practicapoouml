package entornosuml;
public class Delantero extends Futbolista implements Disparo {
    private int tiro;
    private int definicion;

    public Delantero(int tiro, int definicion, String nombre, String apellido, int altura, double peso) {
        super(nombre, apellido, altura, peso);
        this.tiro = tiro;
        this.definicion = definicion;
    }
    
    //<editor-fold defaultstate="collapsed" desc="getters y setters">
     public int getTiro() {
        return tiro;
    }

    public void setTiro(int tiro) {
        this.tiro = tiro;
    }

    public int getDefinicion() {
        return definicion;
    }

    public void setDefinicion(int definicion) {
        this.definicion = definicion;
    }
//</editor-fold>

    public void cambiartiro(int nuevotiro){
        setTiro(nuevotiro);
        System.out.println("Tiro actualizado");
    }
    
    public void cambiardefinicion(int nuevadefinicion){
        setDefinicion(nuevadefinicion);
        System.out.println("Definicion actualizada");
    }
    
    public int puntostotales(){
        int total;
        total=tiro+definicion;
        return total;
    }
    
    
    @Override
    public String toString() {
        return super.toString()+"Delantero{" + "tiro=" + tiro + ", definicion=" + definicion + '}';
    }

    @Override
    public boolean disparoapuerta(boolean tiro) {
        return tiro;
    }

   
}
