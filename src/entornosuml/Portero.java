package entornosuml;
public class Portero extends Futbolista{
    private int paradas;
    private int reflejos;

    public Portero(int paradas, int reflejos, String nombre, String apellido, int altura, double peso) {
        super(nombre, apellido, altura, peso);
        this.paradas = paradas;
        this.reflejos = reflejos;
    }

    //<editor-fold defaultstate="collapsed" desc="getters y setters">
       public int getParadas() {
        return paradas;
    }

    public void setParadas(int paradas) {
        this.paradas = paradas;
    }

    public int getReflejos() {
        return reflejos;
    }

    public void setReflejos(int reflejos) {
        this.reflejos = reflejos;
    }
//</editor-fold>
    
    public void cambiarparadas(int nuevaparadas){
        setParadas(nuevaparadas);
        System.out.println("Paradas actualizadas");
     }
    
    public void cambiarreflejos(int nuevosreflejos){
        setReflejos(nuevosreflejos);
        System.out.println("Reflejos actualizados");
     }
    
    public int puntostotales(){
        int total;
        total=paradas+reflejos;
        return total;
    }

    @Override
    public String toString() {
        return super.toString()+"Portero{" + "paradas=" + paradas + ", reflejos=" + reflejos + '}';
    }
 
    
    
    
    
}
